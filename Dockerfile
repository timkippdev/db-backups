FROM mysql:5.7

RUN apt-get update && \
    apt-get install -y git zip unzip && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN mkdir -p /home/database-backups/database-backups

WORKDIR /home/database-backups

COPY . .

RUN chmod +x database-backup.sh